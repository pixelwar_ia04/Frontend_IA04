# Installation de l'environnement

## Prérequis au lancement de l'app

node version 16.0.0

## Installation des prérequis

Download nvm (Node Versions Manager)

Exemple pour windows: 
 1) https://github.com/coreybutler/nvm-windows/releases
 2) Telécharger nvm-setup.exe puis l'executer

Download la bonne version de node
 1) Ouvrir le terminal
 2) `nvm install 16.0.0`
 3) `nvm use 16.0.0`

## Installation de l'app

 1) Ouvrir le terminal
 2) Rentrer dans la racine de l'app (`cd Frontend_IA04`)
 3) `npm install`

## Lancement de l'app

 1) Ouvrir le terminal
 2) Rentrer dans la racine de l'app (`cd Frontend_IA04`)
 3) `npm start`

## Affichage du canvas

 1) Dans la page web, cliquer du `Personnalisation`
 2) Dans le champ `canvas-id` (tout à gauche), saisir `1`
 3) Cliquer sur `start`
