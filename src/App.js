import './App.css';
import React, { useState, useEffect, useRef } from 'react';
import Draggable from 'react-draggable';
//import axios from 'axios';


function App() {
  const [canvasHeight, setCanvasHeight] = useState(1);
  const [canvasWidth, setCanvasWidth] = useState(1);
  const [zoom, setZoom] = useState(1);
  const [x_value, setX] = useState(0);
  const [y_value, setY] = useState(0);
  const [c_value, setC] = useState("");
  const [started, setStarted] = useState(false);
  const [timeStarted, setTimeStarted] = useState(Date.now());
  const [time, setTime] = useState(Date.now());
  const [perso, setPerso] = useState(false);
  const [placeId, setPlaceId] = useState(0);
  const [init, setInit] = useState(false);


  const canvasRef = useRef(null);

  const canvas = canvasRef ? canvasRef.current : null;
  const ctx = canvas ? canvas.getContext('2d') : null;

  const HOST_URL='http://localhost:5555/'

  useEffect(() => {
    const interval = setInterval(() => setTime(Date.now()), 100);
    return () => {
      clearInterval(interval);
    };
  }, []);

  useEffect(() => {
    if(started){
      if(!init){
        updateFirstRequests()
      }else{

          updateRequests()

      }
    }

  }, [time]);
  
  function updateFirstRequests() {
    
    fetch(HOST_URL+'get_canvas', {
          method: 'POST',
          body: JSON.stringify({
            'place-id':'place'+placeId,
            'reset-diff':true
            }),
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then(response => response.json())
          .then(canvas => {
            setCanvas(canvas)
            setInit(true)
          })
          .catch((error) => {
            console.error(error);
          });
  }

  function updateRequests() {
    fetch(HOST_URL+'get_diff', {
          method: 'POST',
          body: JSON.stringify({'place-id':'place'+placeId}),
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then(response => response.json())
          .then(res => {
            //console.log(res)
            setDiff(res)
          })
          .catch((error) => {
            console.error(error);
          });
  }

  function setDiff(res){
    for (var i=0;i<res.diff.length;i++){
      setColorXY(res.diff[i].x,res.diff[i].y,res.diff[i].c)
    }
  }

  function setCanvas(res){
    console.log("Goiiiiin'")
    setCanvasHeight(res.height)
    setCanvasWidth(res.width)

    for (var y=0;y<res.height;y++){
      for (var x=0;x<res.width;x++){
        ctx.fillStyle = res.grid[x][y]
        ctx.fillRect(x, y, 1, 1)
      }
    }
    console.log("Goiiiiin'")
  }

  function setColorXY(x,y,color){

    ctx.fillStyle = color;
    ctx.fillRect(x, y, 1, 1);
  }
  
function Start(){/*
  fetch(HOST_URL+'new_place', {
    method: 'POST',
    body: JSON.stringify({
      'height': 200,
      'width': 200,
      'cooldown': 1
  }),
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then(response => response.json())
    .then(place => {*/
  console.log('grid init');
  setPlaceId(placeId)
      setStarted(true)
    setTimeStarted(Date.now())
  /*renderTable()
  }).catch((error) => {
    console.error(error);
  });*/
}

  const handleSimpleInputChange = (event) => {

    var setInputField;
    var value = event.target.value;
    switch (event.target.name) {
      case "place":
        setInputField = setPlaceId;
        break;
      case "x":
          setInputField = setX;
          break;
      case "y":
          setInputField = setY;
          break;
      case "color":
          setInputField = setC;
          break;
      default: ;
    }
    setInputField(value);
    console.log(value)
  };

  const activatePerso = () => {
    setPerso(!perso)
  }

  const changeCell = () => {
    setColorXY(x_value,y_value,c_value)
  }

  /*
  const renderTable = () => {
    console.log("ooook")
    var tableau
    var ligne
    tableau = []
    for (let i= 0; i<taille;i++){
      ligne=[]
      for (let j= 0; j<taille;j++){
        ligne.push("#FFFFFF")
      }
      tableau.push(ligne)
    }
    setGrid(tableau)
    setStarted(true)
    setTimeStarted(Date.now())
    //setTime(Date.now())
  }
  */
  window.addEventListener('wheel', function (e) {
    var dir;
    dir = (e.deltaY > 0) ? -0.1 : 0.1;
    setZoom(zoom+dir)

    return;
  });


  return (
    <div className='mainView'>
      <div className="top-layer">
        <div  className="top-layer-top">
          <div className="title-container"><h1>UT/place</h1></div>
          <div className="gestion-base">
<p>Place-id:</p>
          <input
          type="number"
          className="input-text options-item"
          value={placeId}
          name="place"
          placeholder='place-id'
          onChange={event => handleSimpleInputChange(event)}>
        </input>
            <div className="gestion-base-inner-button">
              <div className="start-button" onClick={() => Start()}>{!started && "START"}{started && "INIT"}</div>
            </div>

          {started && 
            <div className="gestion-base-inner-button">
              <div className="time-container"><p>Temps depuis le début:{parseInt((time-timeStarted)/1000)}</p></div>
            </div>}
          </div>
        </div>
        <div className="options-container">
          <div className="options-title" onClick={() => activatePerso()}>
            <h2>Personnalisation</h2>
            </div>
          
          {perso && <div className="options-choices">  
            <p className="options-item">X:</p>
            <input
              type="number"
              className="input-text options-item"
              value={x_value}
              name="x"
              onChange={event => handleSimpleInputChange(event)}>
            </input>
            <p className="options-item">Y:</p>
            <input
              type="number"        
              className="input-text options-item"
              value={y_value}
              name="y"
              onChange={event => handleSimpleInputChange(event)}>
            </input>
            <input     
              className="input-text options-item"        
              value={c_value}
              placeholder="Couleur"
              name="color"
              onChange={event => handleSimpleInputChange(event)}>
            </input>
            <div className="change-cell-button options-item" onClick={() => changeCell()}>Change</div>
          </div>}

        </div>
      </div>
      <div className='gridView-container'>
        {started && <div className='gridView'>
          
            <div id='matrice' className='matrice' style={{transform: 'translateZ('+200*zoom+'px)'}}>
              <Draggable>
                  <canvas className='tbody' ref={canvasRef} id="canvas" width={canvasHeight} height={canvasWidth}></canvas>
              </Draggable>
            </div>

        </div>}
      </div>
    </div>
  );
}

export default App;
