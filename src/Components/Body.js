import '../App.css';
import React, { useState, useEffect, Fragment } from 'react';
import Draggable from 'react-draggable';
import axios from 'axios';


function Body() {
  const taille=100
  const [grid, setGrid] = useState([]);
  const [zoom, setZoom] = useState(1);
  const [x_value, setX] = useState(0);
  const [y_value, setY] = useState(0);
  const [c_value, setC] = useState("");
  const [started, setStarted] = useState(false);
  const [timeStarted, setTimeStarted] = useState(Date.now());
  const [time, setTime] = useState(Date.now());

  useEffect(() => {
    const interval = setInterval(() => setTime(Date.now()), 1000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  /*
  useEffect(() => {
    updateRequests()
  }, [time]);*/

const REACT_APP_API_URL="http://localhost:5555/get_grid"



  function updateRequests() {
    axios.get(REACT_APP_API_URL)
    .then(res => {
        setGrid(res.data[0])
      }).catch(res => {
        console.log("ERROR_REQUETE")
        console.log("res:",res)
    })
  }

  const handleSimpleInputChange = (event) => {

    var setInputField;
    var value = event.target.value;
    switch (event.target.name) {
        case "x":
            setInputField = setX;
            break;
        case "y":
            setInputField = setY;
            break;
        case "color":
            setInputField = setC;
            break;
        default: ;
    }
    setInputField(value);
    console.log(value)
  };


  const changeCell = () => {
    var mat=grid
    mat[x_value][y_value]=c_value
    setGrid(mat)
    console.log(grid)
  }

  const renderTable = () => {
    var tableau
    var ligne
    tableau = []
    for (let i= 0; i<taille;i++){
      ligne=[]
      for (let j= 0; j<taille;j++){
        ligne.push("#FFFFFF")
      }
      tableau.push(ligne)
    }
    setGrid(tableau)
    setStarted(true)
    setTimeStarted(Date.now())
    //setTime(Date.now())
  }
  
  window.addEventListener('wheel', function (e) {
    var dir;
    dir = (e.deltaY > 0) ? -0.1 : 0.1;
    setZoom(zoom+dir)

    return;
  });


  return (
    <div className='mainView'>
      <div className="options-container">
        {started && 
          <Fragment>
            <h2>Personnalisez</h2>
            <p>Temps depuis le début:{parseInt((time-timeStarted)/1000)}</p>
            <p>X:</p>
            <input
              type="number"
              className="input-text"
              value={x_value}
              name="x"
              onChange={event => handleSimpleInputChange(event)}>
            </input>
            <p>Y:</p>
            <input
              type="number"        
              className="input-text"
              value={y_value}
              name="y"
              onChange={event => handleSimpleInputChange(event)}>
            </input>
            <input     
              className="input-text"        
              value={c_value}
              placeholder="Couleur"
              name="color"
              onChange={event => handleSimpleInputChange(event)}>
            </input>
            <div className="change-cell-button" onClick={() => changeCell()}>Change</div>
            <div className="change-cell-button" onClick={() => updateRequests()}>MAJ</div>
          </Fragment>
        }
        {started && <div className="start-button" onClick={() => renderTable()}>INIT</div>}
        {!started && <div className="start-button" onClick={() => renderTable()}>START</div>}
      </div>
      <div className='gridView-container'>
        <div className='gridView'>
          
            <table id='matrice' className='matrice' style={{transform: 'translateZ('+200*zoom+'px)'}}>
            <Draggable>
              <tbody className='matrice-body'>
                {grid.map((ligne, y) => (
                
                  <Fragment key={`${ligne}~${y}`}>
                    <tr>
                    {ligne.map((cellule, x) => (
                      <Fragment key={`${cellule}~${x}`}>
                       <td style={{backgroundColor:cellule}}></td>
                        </Fragment>
                    ))}
                    </tr>
                  </Fragment>

                    
                ))}
              </tbody>
              </Draggable>
            </table>

        </div>
      </div>
    </div>
  );
}

export default Body;
